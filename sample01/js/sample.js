$(document).ready(function(){
    var url = 'http://cs.listenfield.com/OGCAPIV2.jsp?Key=';

    /*
     * formatXML was written by Stuart Powers.
     * URL: https://gist.github.com/sente/1083506
     */
    var formatXML = function(xml) {
	var formatted = '';
	var reg = /(>)(<)(\/*)/g;
	xml = xml.replace(reg, '$1\r\n$2$3');
	var pad = 0;
	$.each(xml.split('\r\n'), function(index, node) {
            var indent = 0;
            if (node.match( /.+<\/\w[^>]*>$/ )) {
		indent = 0;
            } else if (node.match( /^<\/\w/ )) {
		if (pad != 0) {
                    pad -= 1;
		}
            } else if (node.match( /^<\w([^>]*[^\/])?>.*$/ )) {
		indent = 1;
            } else {
		indent = 0;
            }
	    
            var padding = '';
            for (var i = 0; i < pad; i++) {
		padding += '  ';
            }
	    
            formatted += padding + node + '\r\n';
            pad += indent;
	});
	
	return formatted;
    };

    var genURIwithToken = function(){
	return url + $('input').val();
    };

    $('button[name=submit]').on('click',function(event){
	$('#main').css('display','none');
	$('#loading').css('display','block');
	
	$.ajax({
	    type: "POST",
	    url: genURIwithToken(),
	    data: $('textarea[name=query]').val(),
	    timeout: 60000,
	    contentType: 'text/plain; charset=UTF-8',
	    dataType: 'text'
	}).then(
	    function(data){ // Success.
		$('textarea[name=response]').val(formatXML(data));
	    },
	    function(data){ // Failed.
		alert("failed!");
	    }
	).then(
	    function(data){
		$('#main').css('display','block');
		$('#loading').css('display','none');
	    },
	    function(data){
		alert("failed!");
	    }
	);
    });

    $('select#typeOfQuery').on('change',function(){
	var typeName = $('select#typeOfQuery').val();
	if(typeName === ""){
	    $('textarea[name=query]').val("");
	}else if(typeName === "getCapabilities"){
	    $('textarea[name=query]').val(formatXML($('textarea[name=qGetCapabilities]').val()));
	}else if(typeName === "describeSensor"){
	    $('textarea[name=query]').val(formatXML($('textarea[name=qDescribeSensor]').val()));
	}else if(typeName === "getObservation"){
	    $('textarea[name=query]').val(formatXML($('textarea[name=qGetObservation]').val()));
	}
    });

    $('#loading').css('display','none');
});
