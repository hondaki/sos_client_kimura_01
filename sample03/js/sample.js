$(document).ready(function(){
    var url = 'http://cs.listenfield.com/OGCAPIV2.jsp?Key=';

    /*
     * Initialize
     */
    $('#loading').css('display','none'); // div#loadingを非表示
    
    /*
     * formatXML was written by Stuart Powers.
     * URL: https://gist.github.com/sente/1083506
     */
    var formatXML = function(xml) {
	var formatted = '';
	var reg = /(>)(<)(\/*)/g;
	xml = xml.replace(reg, '$1\r\n$2$3');
	var pad = 0;
	$.each(xml.split('\r\n'), function(index, node) {
            var indent = 0;
            if (node.match( /.+<\/\w[^>]*>$/ )) {
		indent = 0;
            } else if (node.match( /^<\/\w/ )) {
		if (pad != 0) {
                    pad -= 1;
		}
            } else if (node.match( /^<\w([^>]*[^\/])?>.*$/ )) {
		indent = 1;
            } else {
		indent = 0;
            }
	    
            var padding = '';
            for (var i = 0; i < pad; i++) {
		padding += '  ';
            }
	    
            formatted += padding + node + '\r\n';
            pad += indent;
	});
	
	return formatted;
    };

    /*
     * データ取得用URLを生成(cS固有)
     */
    var genURIwithToken = function(){
	return url + $('input').val();
    };

    /*
     * Dygraphsでの描画用配列の生成(データ)
     */
    var genArrayforDygraph = function(hArray){
	var data = [];
	
	for(var time in hArray){
	    var tmp = [new Date(time)];
	    for(var i in hArray[time]){
		tmp.push(parseFloat(hArray[time][i]));
	    }
	    data.push(tmp);
	}

	return data;
    };

    /*
     * グラフの描画処理
     */
    var plot = function(csv,lbs){
	var g = new Dygraph(
	    document.getElementById("graph"),
	    csv,
	    {labels: lbs}
	);

	return g;
    }
    
    /*
     * submitボタンが押された時の処理
     */
    $('button[name=submit]').on('click',function(event){
	$('#main').css('display','none'); // div#mainを非表示
	$('#loading').css('display','block'); // div#loadingを表示
	var g; // Dygraphsのオブジェクト
	
	$.ajax({ // データの取得処理(実際のgetObservationが走るのはここ)
	    type: "POST",
	    url: genURIwithToken(),
	    data: $('textarea[name=query]').val(),
	    timeout: 60000,
	    contentType: 'application/xml; charset=UTF-8',
	    dataType: 'text'
	}).then(
	    function(data){ // Success.
		var typeName = $('select#typeOfQuery').val();
		var xml = $(data);
		var tmp = {};
		var tlbs = {"Date":true};
		var lbs = new Array();

		// XMLのパース処理
		xml.find("sos\\:observationData").each(function(idx, elm){
		    var time = $(this).find("gml\\:timePosition").text().substr(0,16); //2018-01-01T00:00:00+0900 -> 2018-01-01T00:00
		    time = time.replace(/T/g," "); // 2018-01-01T00:00 -> 2018-01-01 00:00
		    time = time.replace(/-/g,"/"); // 2018-01-01 00:00 -> 2018/01/01 00:00
		    var data = $(this).find("om\\:result").text();
		    var label = $(this).find("om\\:observedProperty").text().replace(/\"/g,"");
		    label += "[" + $(this).find("om\\:result").attr("uom") + "]";

		    // 取得したデータからハッシュ配列を生成
		    if(!(time in tmp)){
			tmp[time] = new Array();
		    }
		    tmp[time].push(data);
		    tlbs[label] = true;
		});

		// レスポンスの表示処理(textareaへと表示)
		$('textarea[name=response]').val(data);

		// グラフの凡例生成
		for(var key in tlbs){
		    lbs.push(key);
		}
		// グラフの描画
		g = plot(genArrayforDygraph(tmp),lbs);
	    },
	    function(data){ // Failed.
		$('#loading').css('display','none'); // div#loadingを非表示
		$('#main').css('display','block'); // div#mainを表示
		alert("[FAILED]Parse Error!!"); // エラー表示
	    }
	).then(
	    function(data){
		$('#loading').css('display','none'); // div#loadingを非表示
		$('#main').css('display','block'); // div#mainを表示
		g.resize($("#main").outerWidth(),320); // 強制的にグラフをリプロット
	    }
	);
    });
});
