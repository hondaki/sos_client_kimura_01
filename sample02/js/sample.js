$(document).ready(function(){
    var url = 'http://cs.listenfield.com/OGCAPIV2.jsp?Key=';

    /*
     * Initialize.
     */
    $('#loading').css('display','none');
    
    /*
     * formatXML was written by Stuart Powers.
     * URL: https://gist.github.com/sente/1083506
     */
    var formatXML = function(xml) {
	var formatted = '';
	var reg = /(>)(<)(\/*)/g;
	xml = xml.replace(reg, '$1\r\n$2$3');
	var pad = 0;
	$.each(xml.split('\r\n'), function(index, node) {
            var indent = 0;
            if (node.match( /.+<\/\w[^>]*>$/ )) {
		indent = 0;
            } else if (node.match( /^<\/\w/ )) {
		if (pad != 0) {
                    pad -= 1;
		}
            } else if (node.match( /^<\w([^>]*[^\/])?>.*$/ )) {
		indent = 1;
            } else {
		indent = 0;
            }
	    
            var padding = '';
            for (var i = 0; i < pad; i++) {
		padding += '  ';
            }
	    
            formatted += padding + node + '\r\n';
            pad += indent;
	});
	
	return formatted;
    };

    var genURIwithToken = function(){
	return url + $('input').val();
    };

    /*
     * Event Driven.
     */
    $('button[name=submit]').on('click',function(event){
	$('#main').css('display','none');
	$('#loading').css('display','block');
	
	$.ajax({
	    type: "POST",
	    url: genURIwithToken(),
	    data: $('textarea[name=query]').val(),
	    timeout: 60000,
	    contentType: 'text/plain; charset=UTF-8',
	    dataType: 'text'
	}).then(
	    function(data){ // Success.
		var typeName = $('select#typeOfQuery').val();
		var xml = $(data);
		
		if(typeName === "getCapabilities"){
		    var tmp = "";
		    xml.find("sos\\:ObservationOffering").each(function(idx, elm){
			var procedure = $(this).find("swes\\:procedure").text();
			tmp += procedure + "\n";
			$(this).find("swes\\:observableProperty").each(function(idx, elm){
			    tmp += "+ " + $(this).text() + "\n";
			});
		    });
		    $('textarea[name=response]').val(tmp);
		}else if(typeName === "getObservation"){
		    var tmp = "";
		    xml.find("sos\\:observationData").each(function(idx, elm){
			var time = $(this).find("gml\\:timePosition").text();
			var data = $(this).find("om\\:result").text();
			var uom = $(this).find("om\\:result").attr("uom");
			tmp += time + "," + data + "[" + uom + "]\n";
		    });
		    $('textarea[name=response]').val(tmp);
		}else{
		    $('textarea[name=response]').val(data);
		}
	    },
	    function(data){ // Failed.
		alert("[FAILED]Parse Error!!");
	    }
	).then(
	    function(data){
		$('#main').css('display','block');
		$('#loading').css('display','none');
	    }
	);
    });

    $('select#typeOfQuery').on('change',function(){
	var typeName = $('select#typeOfQuery').val();
	if(typeName === ""){
	    $('textarea[name=query]').val("");
	}else if(typeName === "getCapabilities"){
	    $('textarea[name=query]').val(formatXML($('textarea[name=qGetCapabilities]').val()));
	}else if(typeName === "describeSensor"){
	    $('textarea[name=query]').val(formatXML($('textarea[name=qDescribeSensor]').val()));
	}else if(typeName === "getObservation"){
	    $('textarea[name=query]').val(formatXML($('textarea[name=qGetObservation]').val()));
	}
    });
});
